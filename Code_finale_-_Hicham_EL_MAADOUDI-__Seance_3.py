import asyncio
from bleak import BleakClient, BleakError
from bleak import BleakScanner

async def callback_handler(sender, data):
    # Gérez les données de notification reçues ici
    print(f"Notification reçue : {data.hex()}")

async def read_characteristics(address):
    try:
        async with BleakClient(address, timeout=2000.0) as client:
            services = await client.get_services()

            for service in services:
                print(f"Service UUID: {service.uuid}")
                for characteristic in service.characteristics:
                    print(f"Characteristic UUID: {characteristic.uuid}")
                    try:
                        data = await client.read_gatt_char(characteristic)
                        print("Read data:", data)
                    except BleakError as e:
                        print(f"Error reading characteristic {characteristic.uuid}: {e}")

            # Abonnez-vous à l'UUID 1b0d1302-a720-f7e9-46b6-31b601c4fca1
            uuid_to_subscribe = "1b0d1304-a720-f7e9-46b6-31b601c4fca1"
            await client.start_notify(uuid_to_subscribe, callback_handler)
            print("Abonnement aux notifications a commencé")

            # Écrivez la chaîne spécifiée dans la caractéristique avec l'UUID "1b0d1304-a720-f7e9-46b6-31b601c4fca1"
            uuid_to_write = "1b0d1304-a720-f7e9-46b6-31b601c4fca1"
            file_request = b'T25\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x27\x0F'
            value_to_write = bytearray(file_request)
            await client.write_gatt_char(uuid_to_write, value_to_write, True)
            print("Chaîne écrite avec succès !")

            # Attendez les notifications (vous pouvez ajouter votre propre logique ici)
            await asyncio.sleep(10)
            print("Attente terminée")

            await client.stop_notify(uuid_to_subscribe)
            print("Abonnement aux notifications arrêté")

    except BleakError as e:
        print(f"Error occurred while connecting to {address}: {e}")

async def main():
    try:
        devices = await BleakScanner.discover()
        for d in devices:
            print(d)

        device_address = "FC:7D:37:BE:AC:92"

        await read_characteristics(device_address)

    except BleakError as e:
        print(f"Error occurred while scanning for devices: {e}")

asyncio.run(main())
